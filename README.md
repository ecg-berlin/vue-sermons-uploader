# Vue-Sermon-Uploader

![Logo](images/banner.png "Logo")

This is a script to parse file uploaded via ftp and feed them to https://gitlab.com/ecg-berlin/vue-sermons

## Development

### Prepare

You need docker.
Installation guide for

-   [MacOSX](https://docs.docker.com/docker-for-mac/install/)
-   [Windows](https://docs.docker.com/docker-for-windows/install/)

Linux. See [first here](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

```bash
# convenience script:
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

### Run

Run `sh run-local.sh`
The code for the webserver is found here: [ecg-berlin/vue-sermons](https://gitlab.com/ecg-berlin/vue-sermons)

### Linting

We use rubocop for linting. Install it via `gem install rubocop`

Run `rubocop` for list of problems or `rubocop -a` to fix them.

## Deployment

To deploy vue-sermons-upload you can simply run:

```bash
sudo docker run \
    -v '/var/www/vhosts/example.com/httpdocs/web/downloads/add:/usr/data/add' \
    -v '/var/www/vhosts/example.com/httpdocs/web/downloads/backup:/usr/data/backup' \
    -v '/var/www/vhosts/example.com/httpdocs/web/downloads/new:/usr/data/input' \
    -v '/var/www/vhosts/example.com/httpdocs/web/downloads/predigten:/usr/data/output' \
    --name sermons -dit 'registry.gitlab.com/ecg-berlin/vue-sermons-uploader/stable'
```

or use an ansible script like this

```yaml
---
- hosts: example.com
  tasks:
      - name: Remove vue-sermon-uploader container
        docker_container:
            name: sermons
            state: absent

      - name: Start the vue-sermons-uploader container
        docker_container:
            name: sermons
            image: registry.gitlab.com/ecg-berlin/vue-sermons-uploader/stable
            state: started
            restart_policy: "unless-stopped"
            pull: yes
            volumes:
                - "/var/www/vhosts/example.com/httpdocs/web/downloads/backup:/usr/data/backup"
                - "/var/www/vhosts/example.com/httpdocs/web/downloads/predigten:/usr/data/output"
                - "/var/www/vhosts/example.com/httpdocs/web/downloads/new:/usr/data/input"
                - "/var/www/vhosts/example.com/httpdocs/web/downloads/add:/usr/data/add"
            env:
                sermonApiUrl: https://example.com
                sermonSecretKey: "secret_key"
                sermonPublicPath: //example.com/downloads/predigten
                sermonChown: "10001:1002"
                datadogToken: "datadog_api_key"

```
