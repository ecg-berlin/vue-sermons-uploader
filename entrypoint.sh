#!/bin/bash
echo "Docker sermons container has been started"
declare -p | grep -Ev 'BASHOPTS|BASH_VERSINFO|EUID|PPID|SHELLOPTS|UID' > /usr/data/container.env
chmod 0644 /etc/cron.d/sermons-cron
crontab /etc/cron.d/sermons-cron
cron -f