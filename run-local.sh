#!/bin/bash
set -e
sudo service docker start
sudo docker build -t registry.gitlab.com/ecg-berlin/vue-sermons-uploader/stable .
sudo docker rm --force sermons-test && true
sudo mkdir -p /opt/testhost/input/Predigt/de/Testpredigt/
sudo rm -rf /opt/testhost/input/Predigt/de/Testpredigt/*
sudo touch "/opt/testhost/input/Predigt/de/Testpredigt/2021-01-01 = 2.Mose 1,1 = Test Prediger.mp3"
sudo touch "/opt/testhost/input/Predigt/de/Testpredigt/.DSTORE"
sudo docker run \
    -v '/opt/testhost/add:/usr/data/add' \
    -v '/opt/testhost/backup:/usr/data/backup' \
    -v '/opt/testhost/input:/usr/data/input' \
    -v '/opt/testhost/output:/usr/data/output' \
    -a stdin -a stdout \
    --entrypoint ruby \
    --name sermons-test -i -t 'registry.gitlab.com/ecg-berlin/vue-sermons-uploader/stable'  /usr/src/sermons/src/cron.rb

echo "Data is under /opt/testhost"