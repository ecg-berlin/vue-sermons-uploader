#!/bin/bash
set -e
sudo service docker start
sudo docker build -t registry.gitlab.com/ecg-berlin/vue-sermons-uploader/stable .
sudo docker rm --force sermons-test && true
echo "Data is under /opt/testhost"
sudo docker run \
    -v '/opt/testhost/add:/usr/data/add' \
    -v '/opt/testhost/backup:/usr/data/backup' \
    -v '/opt/testhost/input:/usr/data/input' \
    -v '/opt/testhost/output:/usr/data/output' \
    -v '/opt/testhost/log:/usr/data/log' \
    --name sermons-test -d -i -t 'registry.gitlab.com/ecg-berlin/vue-sermons-uploader/stable'
echo "Docker container is running…"
echo "\nsudo docker exec -it sermons-test /bin/bash\n"
sudo docker ps