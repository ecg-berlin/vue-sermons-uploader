# frozen_string_literal: true

require 'rest_client'
require 'json'
require 'securerandom'
require 'openssl'
require_relative 'ref'

def group_files(file_names)
  ret = Hash[
      'other' => [],
      'audio' => [],
      'video' => [],
      'image' => []
  ]
  file_names.each do |file_name|
    file_type = 'other'
    suffix = File.extname(file_name).downcase
    if (suffix == '.mp3') || (suffix == '.ogg')
      file_type = 'audio'
    elsif suffix == '.mp4'
      file_type = 'video'
    elsif (suffix == '.jpg') || (suffix == '.jpeg') || (suffix == '.png')
      file_type = 'image'
    end
    file_name[$options[:path_result_files]] = ''
    ret[file_type] << $options[:public_path] + file_name
  end
  ret
end

def api_register_files(api_url, file_info)
  random = SecureRandom.urlsafe_base64
  digest = OpenSSL::Digest.new('sha1')
  hash = OpenSSL::HMAC.hexdigest(digest, $options[:secret_key], random)

  data = Hash[
      title: convert_to_utf8(file_info[:title]),
      language: file_info[:language],
      groupCode: file_info[:group_code],
      speaker: convert_to_utf8(file_info[:speaker]),
      date: file_info[:date],
      duration: file_info[:duration],
      seriesName: convert_to_utf8(file_info[:serie]),
      scripture: file_info[:ref],
      files: group_files(file_info[:remote_file_names]),
      random: random,
      hash: hash,
  ]
  begin
    pp data
    RestClient.post api_url + '/sermon/create', data.to_json.to_s, 'Content-Type' => 'application/json'
  rescue StandardError => e
    $logger.warn e.response
    $logger.warn e.to_str
  end
end

def api_get_sermons(api_url)
  data = JSON.parse(RestClient.get(api_url + '/sermon/list?limit=50', 'Content-Type' => 'application/json').body)
  data['sermons']
rescue StandardError => e
  $logger.warn e.response
  $logger.warn e.to_str
end

def api_register_add_files(api_url, sermon_id, files)
  random = SecureRandom.urlsafe_base64
  digest = OpenSSL::Digest.new('sha1')
  hash = OpenSSL::HMAC.hexdigest(digest, $options[:secret_key], random)
  data = Hash[
      files: group_files(files),
      random: random,
      hash: hash,
  ]
  begin
    response = RestClient.post(api_url + '/sermon/add?id=' + sermon_id, data.to_json.to_s, 'Content-Type' => 'application/json')
    puts 'resp:' + response.body
  rescue StandardError => e
    puts e.to_s
    $logger.warn e.to_s
  end
end
