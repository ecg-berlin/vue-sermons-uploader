# frozen_string_literal: true

def remote_path(local, group, date)
  ext = File.extname(local)
  type =
    case ext.downcase
    when '.mp3'
      'audio'
    when '.ogg'
      'audio'
    when '.opus'
      'audio'
    when '.aac'
      'audio'
    when '.mp4'
      'video'
    else
      'other'
    end

  year = Date.parse(date).year.to_s
  $options[:path_result_files] + "/#{group}/#{type}/#{year}"
end

# iterate files and copies them to remote_path
# returns an array with the new paths
def move_files(files, group, date)
  remote = []
  files.each do |file|
    next if File.basename(file).start_with? '.'

    full = remote_path(file, group, date) + '/' + File.basename(file)
    copy_file(file, full)
    remote << full
  end

  remote
end
