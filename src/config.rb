# frozen_string_literal: true

require 'optparse'
require_relative 'strings'
require 'yaml'

def parse_options
  options = {}
  config = {}

  config[:api_url] = ENV['sermonApiUrl']
  config[:secret_key] = ENV['sermonSecretKey']
  config[:secret_key] ||= 'asd'
  config[:public_path] = ENV['sermonPublicPath']
  config[:chown] = ENV['sermonChown']

  OptionParser.new do |opts|
    opts.banner = 'Usage: cron.rb [options]'
    options.merge! config

    options[:slow] = false
    options[:path_tmp] = '/usr/data/tmp'
    options[:path_log] = '/usr/data/log'
    options[:path_incoming_files] = '/usr/data/input'
    options[:path_result_files] = '/usr/data/output'
    options[:path_backup] = '/usr/data/backup'
    options[:path_add_files] = '/usr/data/add'

    options[:locale] = 'de'

    opts.on('--slow', 'Wait for ftp uploads') do |_file|
      options[:slow] = true
    end

    opts.on('--log path', 'Path to logs') do |path|
      options[:path_log] = path
    end

    opts.on('-a', '--api PATH', 'Path zur API') do |url|
      options[:api_url] = url
    end

    # This displays the help screen, all programs are
    # assumed to have this option.
    opts.on('-h', '--help', 'Display this screen') do
      puts opts
      exit
    end
  end.parse!

  options
end

def error_check(hash, needed_options)
  needed_options.each do |x|
    if hash[x].nil?
      puts "Variable #{x} is missing in config"
      return :failed
    end
  end
  :ok
end

def error_check_options(hash)
  error_check(hash, [:api])
end

def error_check_file(hash)
  return :failed if error_check(hash, %i[title speaker date group_code files]) == :failed

  hash[:files].each do |x|
    unless File.exist? x
      puts "Die Datei #{x} existiert nicht"
      return :failed
    end
  end
  :ok
end
