# frozen_string_literal: true

require 'taglib'

def writeid3_mp3(file_name, file_info)
  ref_parser = RefParser.new
  frame_factory = TagLib::ID3v2::FrameFactory.instance
  frame_factory.default_text_encoding = TagLib::String::UTF8
  TagLib::MPEG::File.open file_name do |file|
    $logger.debug 'writing mp3 tags' + file_name

    tag = file.id3v2_tag
    tag.album = if file_info.key? :serie
                  file_info[:serie]
                else
                  file_info[:group_code]
                end

    tag.genre = file_info[:group_code]
    tag.year = Date.parse(file_info[:date]).year
    tag.comment = $options[:tagComment]
    tag.artist = file_info[:speaker]
    tag.title = if ref_parser.valid_ref? file_info[:ref]
                  "#{ref_parser.refs_normalize(file_info[:ref], file_info[:language])} #{file_info[:title]}"
                else
                  file_info[:title]
                end

    $logger.debug file_info[:files]
    file_info[:files].each do |cover_file|
      $logger.debug 'attach image to sermon: ' + File.extname(cover_file).downcase

      next unless image? cover_file

      $logger.debug 'attach image to sermon: ' + cover_file
      apic = TagLib::ID3v2::AttachedPictureFrame.new
      apic.mime_type = 'image/jpeg'
      apic.description = 'Cover'
      apic.type = TagLib::ID3v2::AttachedPictureFrame::FrontCover
      apic.picture = File.open(cover_file, 'rb', &:read)

      tag.add_frame(apic)
    end

    file.save
  end
rescue StandardError => e
  puts 'Writing tags failed'
  puts e
  $logger.warn "writing id3-tags failed on #{file_name}"
  $logger.warn e
end
