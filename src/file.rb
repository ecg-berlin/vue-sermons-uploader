# frozen_string_literal: true

require_relative 'ref'
require_relative 'audio'
require_relative 'video'

require 'fileutils'

def copy_file(old, new)
  dir = File.dirname new
  FileUtils.mkdir_p dir unless File.exist? dir

  FileUtils.cp old, new
  FileUtils.chmod 0o644, new
end

def image?(file)
  (File.extname(file).downcase == '.jpg') || (File.extname(file).downcase == '.jpeg') || (File.extname(file).downcase == '.png')
end

# gets a Path to a file and some info about it, and renames it to be pretty
def rename(file_name, file_info)
  # ref = ""
  # if is_valid_ref?(file_info[:ref])
  # ref = "["+refs_normalize(file_info[:ref], file_info[:lang]) + "] "
  # end
  # TODO: Use the ref

  suffix = File.extname(file_name).downcase
  dir = File.dirname file_name
  new_file_name = if (suffix == '.mp3') || (suffix == '.ogg') || (suffix == '.mp4')
                    dir +
                      "/#{file_info[:date]}_#{more_clean(file_info[:title])}_#{more_clean(file_info[:speaker])}" +
                      suffix

                  else
                    dir + '/' + clean(File.basename(file_name))
                  end
  File.rename(file_name, new_file_name)
  new_file_name
end

def prepare_files(file_info)
  prepared_files = []
  file_info[:files].each do |file_name|
    suffix = File.extname(file_name).downcase
    writeid3_mp3(file_name, file_info) if suffix == '.mp3'
    write_metadata_mp4(file_name, file_info) if suffix == '.mp4'
    prepared_files << rename(file_name, file_info)
  end
  prepared_files
end

# returns all files in a folder (skip all folders)
def find_all_files(path)
  files = []
  Dir.foreach(path) do |item|
    next if (item == '.') || (item == '..')

    next unless File.file?(path + '/' + item)

    files << path + '/' + item
  end
  files
end

# clears a folder
def clear_folder(folder)
  FileUtils.rm_rf folder if File.exist? folder
  Dir.mkdir folder
end

# deletes a series of folders
def delete_folders(folders)
  folders.each do |folder|
    if File.exist? folder
      $logger.debug "delete #{folder}"
      FileUtils.rm_rf(folder)
    end
  end
end

def chown(uid_guid, path)
  system "chown #{uid_guid} '#{path}' -R"
end
