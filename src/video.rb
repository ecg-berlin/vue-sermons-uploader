# frozen_string_literal: true

require 'taglib'

def write_metadata_mp4(file_name, file_info)
  ref_parser = RefParser.new
  frame_factory = TagLib::MP4::FrameFactory.instance
  frame_factory.default_text_encoding = TagLib::String::UTF8
  TagLib::MP4::File.open file_name do |file|
    tag = file.tag
    tag.setAlbum(file_info[:group_code])
    tag.setYear(Date.parse(file_info[:date]).year)
    tag.setComment($options[:tagComment])
    tag.setArtist(file_info[:speaker])
    if ref_parser.valid_ref? file_info[:ref]
      tag.setTitle("#{ref_parser.refs_normalize(file_info[:ref], file_info[:lang])} #{file_info[:title]}")
    else
      tag.setTitle(file_info[:title])
    end

    file.save
  end
rescue StandardError
  $logger.warn "writing tags failed on #{file_name}"
end

def parse_videos(path, file_info)
  $logger.debug "parse videos #{path}"
  Dir.foreach(path) do |i|
    ii = path + '/' + i
    next if (i == '.') || (i == '..')

    if File.extname(ii) == '.mp4'
      $logger.debug `qtfaststart '#{ii}' '#{path + '/res2.mp4'}'`
      if File.exist? path + '/res2.mp4'
        $logger.debug `chmod +r '#{path + '/res2.mp4'}'`
        file_info[:files] << path + '/res2.mp4'
      else
        file_info[:files] << ii
        $logger.warn "qtfaststart failed #{path}"
      end

      return :yes
    end
  end
  :no
end
