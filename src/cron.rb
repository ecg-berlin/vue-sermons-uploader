# frozen_string_literal: true

require 'rubygems'
require 'time'
require 'fileutils'

require_relative 'config'
require_relative 'move'
require_relative 'file'
require_relative 'video'
require_relative 'register'
# require_relative 'youtube'

def make_backup(path, file_info)
  b = $options[:path_backup] + '/' + file_info[:group_code] + '/'
  b = b + file_info[:serie] + '/' if file_info.key? :serie
  FileUtils.mkpath(b)
  FileUtils.cp_r(path, b)
end

# a folder
def add_file(path)
  file_info = Hash[]
  files = find_all_files(path)
  files.reject! { |f| File.extname(f).downcase == '.mp4' }
  mp3s = files.select { |f| File.extname(f).downcase == '.mp3' }
  return nil if mp3s.empty?

  mp3 = mp3s.first

  reg = %r{/([^/=]+)/([^/=]+)/([^/=]+)/([^/=]+)(\s*)=(\s*)([^/=]+)(\s*)=(\s*)([^/)=]+).mp3}
  # groupCode/lang/title/date = ref = preacher.mp3
  reg_serie = %r{/([^/=]+)/([^/=]+)/([^/=]+)/([^/=]+)/([^/=]+)(\s*)=(\s*)([^/=]+)(\s*)=(\s*)([^/)=]+).mp3}
  #      groupCode          /lang       /serie      /title  /  date         =       ref         =  preacher.mp3

  mp3_ = mp3.dup
  mp3_[$options[:path_incoming_files]] = ''

  puts mp3_
  if reg_serie =~ mp3_
    y = mp3_.scan(reg_serie)[0]
    file_info[:group_code] = y[0]
    file_info[:language] = y[1]
    file_info[:serie] = y[2]
    file_info[:title] = y[3].strip
    file_info[:date] = y[4].strip
    file_info[:ref] = y[7].strip
    file_info[:speaker] = fix_speaker y[10]
  elsif reg =~ mp3_
    y = mp3_.scan(reg)[0]
    file_info[:group_code] = y[0]
    file_info[:language] = y[1]
    file_info[:title] = y[2].strip
    file_info[:date] = y[3].strip
    file_info[:ref] = y[6].strip
    file_info[:speaker] = fix_speaker y[9]
  else
    return nil
  end
  file_info[:mp3] = mp3
  file_info[:duration] = `ffprobe -i '#{mp3}' -show_entries format=duration -v quiet -of csv="p=0"`.strip.to_i
  file_info[:files] = files
  make_backup(path, file_info)
  file_info
end

def active_upload?(path1, path2)
  f1 = `du -s "#{path1}"`
  f2 = `du -s "#{path2}"`

  sleep 10
  s1 = `du -s "#{path1}"`
  s2 = `du -s "#{path2}"`

  ((s1 != f1) || (s2 != f2))
end

def main
  puts 'Run cron'
  folders_to_delete = []
  $options = parse_options
  FileUtils.mkdir_p $options[:path_log]
  $logger = Logger.new($options[:path_log] + '/sermon_logger.log')

  return -1 if error_check($options, %i[api_url path_result_files path_incoming_files path_add_files path_backup]) == :failed

  FileUtils.mkdir_p $options[:path_result_files]
  FileUtils.mkdir_p $options[:path_incoming_files]
  FileUtils.mkdir_p $options[:path_add_files]
  FileUtils.mkdir_p $options[:path_backup]

  # check if there is an active upload and stop. this means try again after 5 minutes
  if $options[:slow]
    exit 0 if active_upload? $options[:path_incoming_files], $options[:path_add_files]
  end

  # Add new sermons
  Dir.glob($options[:path_incoming_files] + '/**/*').each do |item|
    # scan all folders
    next if (item == '.') || (item == '..')
    next unless File.directory? item # only directories

    puts 'checking ' + item

    file_info = add_file(item)
    pp file_info
    next if file_info.nil?
    next if error_check_file(file_info) == :failed

    $logger.debug 'adding folder ' + item

    folder_for_image = File.expand_path('..', item)
    files = find_all_files folder_for_image
    files.each do |file|
      next unless image? file

      new_filename = item + '/' + File.basename(file)
      copy_file file, new_filename
      file_info[:files] << new_filename
    end

    # check first for videos
    parse_videos(item, file_info)

    prepared_files = prepare_files file_info
    file_info[:remote_file_names] = move_files prepared_files, file_info[:group_code], file_info[:date]
    api_register_files $options[:api_url], file_info

    folders_to_delete << item
  end

  # Create latest 50 sermons folders to add additional files
  current_sermon_paths = []
  sermons = api_get_sermons($options[:api_url])
  sermons.each do |sermon|
    path = $options[:path_add_files] + '/' + '[' + sermon['id'].to_s + '] ' + sermon['title']
    current_sermon_paths << path
    FileUtils.mkdir_p path
    files = find_all_files path
    unless files.empty?
      file_info = Hash[]
      file_info[:group_code] = sermon['groupCode']
      file_info[:language] = sermon['language']
      file_info[:serie] = sermon['seriesName']
      file_info[:title] = sermon['title']
      file_info[:date] = sermon['utcDate']
      file_info[:speaker] = sermon['speaker']
      file_info[:files] = files
      prepared_files = prepare_files file_info
      result_files = move_files prepared_files, sermon['groupCode'], sermon['utcDate']
      api_register_add_files $options[:api_url], sermon['id'].to_s, result_files
    end
    clear_folder path
  end

  sermons.each do |sermon|
    next if sermon['video'].empty?
    next unless sermon['youtube'].nil? || sermon['youtube'].empty?
  end

  Dir.glob($options[:path_add_files] + '/**/*').each do |item| # scan all folders
    next if (item == '.') || (item == '..')
    next unless File.directory? item # only directories

    folders_to_delete << item unless current_sermon_paths.include? item
  end
  unless $options[:chown].nil? || $options[:chown].empty?
    chown $options[:chown], $options[:path_result_files]
    chown $options[:chown], $options[:path_incoming_files]
    chown $options[:chown], $options[:path_add_files]
    chown $options[:chown], $options[:path_backup]
  end

  delete_folders(folders_to_delete)
end

main
