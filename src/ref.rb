# frozen_string_literal: true

# parses bible references to code
class RefParser
  @language = 'de'
  BOOKS = Hash[
      'de' => Hash[0 => ['1Mo', '1.Mose'],
                   1 => ['2Mo', '2.Mose'],
                   2 => ['3Mo', '3.Mose'],
                   3 => ['4Mo', '4.Mose'],
                   4 => ['5Mo', '5.Mose'],
                   5 => %w[Jos Josua],
                   6 => %w[Ri Richter],
                   7 => %w[Ru Rut],
                   8 => ['1Sam', '1.Samuel'],
                   9 => ['2Sam', '2.Samuel'],
                   10 => ['1Kön', '1.Könige'],
                   11 => ['2Kön', '2.Könige'],
                   12 => ['1Chr', '1.Chronik'],
                   13 => ['2Chr', '2.Chronik'],
                   14 => %w[Esra Esr],
                   15 => %w[Nehemia Ne Neh],
                   16 => %w[Ester Es],
                   17 => %w[Hiob Hi],
                   18 => %w[Ps Psalm],
                   19 => %w[Spr Sprüche],
                   20 => %w[Pred Prediger],
                   21 => %w[Hohelied Hoh],
                   22 => %w[Jes Jesaja],
                   23 => %w[Jer Jeremia],
                   24 => %w[Kla Klagelieder],
                   25 => %w[Hes Hesekiel],
                   26 => %w[Dan Daniel],
                   27 => %w[Hosea Hos],
                   28 => %w[Joel Joe],
                   29 => %w[Amos Am],
                   30 => %w[Obadja Ob],
                   31 => %w[Jona Jon],
                   32 => %w[Micha Mic],
                   33 => %w[Nahum Na],
                   34 => %w[Habakuk Hab],
                   35 => %w[Zefanja Zef],
                   36 => %w[Haggai Hag],
                   37 => %w[Sacharja Sac],
                   38 => %w[Mal Maleachi],
                   39 => %w[Mt Matthäus],
                   40 => %w[Mk Markus],
                   41 => %w[Lk Lukas],
                   42 => %w[Joh Johannes],
                   43 => %w[Apg Apostelgeschichte],
                   44 => %w[Röm Römer],
                   45 => ['1Kor', '1.Korinther'],
                   46 => ['2Kor', '2.Korinther'],
                   47 => %w[Gal Galater],
                   48 => %w[Eph Epheser],
                   49 => %w[Phil Philipper],
                   50 => %w[Kol Kolosser],
                   51 => ['1Th', '1.Thessalonicher', '1Thess'],
                   52 => ['2Th', '2.Thessalonicher', '2Thess'],
                   53 => ['1Tim', '1.Timotheus'],
                   54 => ['2Tim', '2.Timotheus'],
                   55 => %w[Tit Titus],
                   56 => %w[Philemon Phm],
                   57 => %w[Heb Hebräer],
                   58 => %w[Jak Jakobus],
                   59 => ['1Petr', '1.Petrus'],
                   60 => ['2Petr', '2.Petrus'],
                   61 => ['1Joh', '1.Johannes'],
                   62 => ['2Joh', '2.Johannes'],
                   63 => ['3Joh', '3.Johannes'],
                   64 => %w[Jud Judas],
                   65 => %w[Offb Offenbarung]],
      'ru' => Hash[0 => ['Бытие'],
                   1 => ['Исход'],
                   2 => ['Левит'],
                   3 => ['Числа'],
                   4 => ['Второзаконие'],
                   5 => ['Иисус Навин'],
                   6 => ['Книга Судей'],
                   7 => ['Руфь'],
                   8 => ['1-я Царств'],
                   9 => ['2-я Царств'],
                   10 => ['3-я Царств'],
                   11 => ['4-я Царств'],
                   12 => ['1-я Паралипоменон'],
                   13 => ['2-я Паралипоменон'],
                   14 => ['Ездра'],
                   15 => ['Неемия'],
                   16 => ['Есфирь'],
                   17 => ['Иов'],
                   18 => ['Псалтирь'],
                   19 => ['Притчи'],
                   20 => ['Екклесиаст'],
                   21 => ['Песни Песней'],
                   22 => ['Исаия'],
                   23 => ['Иеремия'],
                   24 => ['Плач Иеремии'],
                   25 => ['Иезекииль'],
                   26 => ['Даниил'],
                   27 => ['Осия'],
                   28 => ['Иоиль'],
                   29 => ['Амос'],
                   30 => ['Авдия'],
                   31 => ['Иона'],
                   32 => ['Михей'],
                   33 => ['Наум'],
                   34 => ['Аввакум'],
                   35 => ['Софония'],
                   36 => ['Аггей'],
                   37 => ['Захария'],
                   38 => ['Малахия'],
                   39 => ['От Матфея'],
                   40 => ['От Марка'],
                   41 => ['От Луки'],
                   42 => ['От Иоанна'],
                   43 => ['Деяния'],
                   44 => ['К Римлянам'],
                   45 => ['1-е Коринфянам'],
                   46 => ['2-е Коринфянам'],
                   47 => ['К Галатам'],
                   48 => ['К Ефесянам'],
                   49 => ['К Филиппийцам'],
                   50 => ['К Колоссянам'],
                   51 => ['1-е Фессалоникийцам'],
                   52 => ['2-е Фессалоникийцам'],
                   53 => ['1-е Тимофею'],
                   54 => ['2-е Тимофею'],
                   55 => ['К Титу'],
                   56 => ['К Филимону'],
                   57 => ['К Евреям'],
                   58 => ['Иакова'],
                   59 => ['1-e Петра'],
                   60 => ['2-e Петра'],
                   61 => ['1-e Иоанна'],
                   62 => ['2-e Иоанна'],
                   63 => ['3-e Иоанна'],
                   64 => ['Иуда'],
                   65 => ['Откровение']]]

  REF_TYPE4 = /(\p{N}*\p{Punct}*\p{Blank}*\p{Word}+)\s(\d+)-(\d+)/.freeze # Book Chapter-Chapter
  REF_TYPE3 = /(\p{N}*\p{Punct}*\p{Blank}*\p{Word}+)\s(\d+),(\d+)-(\d+),(\d+)/.freeze # Book Chaper,Verse-Chaper,Verse
  REF_TYPE2 = /(\p{N}*\p{Punct}*\p{Blank}*\p{Word}+)\s(\d+),(\d+)-(\d+)/.freeze # Bookm Chaper,Verse-Verse
  REF_TYPE1 = /(\p{N}*\p{Punct}*\p{Blank}*\p{Word}+)\s(\d+),(\d+)/.freeze # Book Chapter,Verse
  REF_TYPE0 = /(\p{N}*\p{Punct}*\p{Blank}*\p{Word}+)\s(\d+)/.freeze # Book Chaper

  def initialize
    @language = $options[:locale]
    raise 'no good language' unless (@language == 'de') || (@language == 'ru')
  end

  # bookname to book id
  def book_id(book_name)
    raise 'no books' if BOOKS.nil?
    raise 'no books for this language' unless BOOKS.key? @language

    BOOKS[@language].each do |id, book_names|
      book_names.each do |name|
        return id + 1 if normalize_book_name(name) == normalize_book_name(book_name)
      end
    end
    puts "Api::book_id didn't found #{book_name} in books"
    nil
  end

  def normalize_book_name(name)
    name.gsub('.', '').gsub(' ', '').strip
  end

  def get_book_name(book_name, lang = $options[:locale])
    raise 'wrong lang: nil' if lang.nil?
    raise 'wrong lang: is an array' if lang.is_a?(Array)
    raise "no books of lang #{lang} " unless BOOKS.key? lang

    id = book_id(book_name) - 1

    raise "out of index #{book_name}" if id.negative? || (id > BOOKS[lang].length)

    # puts "Api::get_book_name [#{lang.first}][#{book_id(book_name)}][0]";
    @books[lang][id][0]
  end

  def valid_book?(matches)
    return false if matches.nil?
    return true unless book_id(matches[0]).nil?
  end

  def get_matches(reg, ref)
    return ref.scan(reg)[0] if reg =~ ref

    nil
  end

  def valid?(reg, ref)
    valid_book?(get_matches(reg, ref))
  end

  def valid_refs?(refs)
    return false if ref.nil?
    return false if ref == ''

    refs.each do |x|
      return false unless valid_ref?(x.strip)
    end

    true
  end

  def valid_ref?(ref)
    return true if valid?(REF_TYPE4, ref)
    return true if valid?(REF_TYPE3, ref)
    return true if valid?(REF_TYPE2, ref)
    return true if valid?(REF_TYPE1, ref)
    return true if valid?(REF_TYPE0, ref)

    false
  end

  def create_hash(book, chapter_start, verse_start, chapter_end, verse_end, ref)
    Hash[
        book: book.to_i,
        chapterStart: chapter_start.to_i,
        verseStart: verse_start.to_i,
        chapterEnd: chapter_end.to_i,
        verseEnd: verse_end.to_i,
        text: refs_normalize(ref)
    ]
  end

  def ref_data(ref)
    t4 = get_matches(REF_TYPE4, ref)
    t3 = get_matches(REF_TYPE3, ref)
    t2 = get_matches(REF_TYPE2, ref)
    t1 = get_matches(REF_TYPE1, ref)
    t0 = get_matches(REF_TYPE0, ref)
    return create_hash(book_id(t4[0]), t4[1], '0', t4[2], '0', ref) unless t4.nil?
    return create_hash(book_id(t3[0]), t3[1], t3[2], t3[3], t3[4], ref) unless t3.nil?
    return create_hash(book_id(t2[0]), t2[1], t2[2], '0', t2[3], ref) unless t2.nil?
    return create_hash(book_id(t1[0]), t1[1], t1[2], '0', '0', ref) unless t1.nil?
    return create_hash(book_id(t0[0]), t0[1], '0', '0', '0', ref) unless t0.nil?

    Hash[text: ref]
  end

  def refs_data(ref)
    ref.split(';')
       .map { |x| ref_data(x.strip) }
       .delete_if(&:nil?)
  end

  def refs_normalize(ref, lang = $options[:locale])
    raise 'wrong lang: nil' if lang.nil?
    raise 'wrong lang: is an array' if lang.is_a?(Array)

    ref.split(';')
       .map { |x| ref_normalize(x.strip, lang) }
       .join('; ')
  end

  def ref_normalize(ref, lang)
    raise 'wrong lang: nil' if lang.nil?
    raise 'wrong lang: is an array' if lang.is_a?(Array)

    if @ref_type4 =~ ref # BookName 1,1-2,12
      y = ref.scan(REF_TYPE4)
      x = y[0]
      return "#{get_book_name(x[0], lang)} #{x[1]}-#{x[2]}"
    end
    if @ref_type3 =~ ref # BookName 1,1-2,12
      y = ref.scan(REF_TYPE3)
      x = y[0]
      return "#{get_book_name(x[0], lang)} #{x[1]},#{x[2]}-#{x[3]},#{x[4]}"
    end

    if @ref_type2 =~ ref # BookName 1,1-12
      y = ref.scan(REF_TYPE2)
      x = y[0]
      return "#{get_book_name(x[0], lang)} #{x[1]},#{x[2]}-#{x[3]}"
    end

    if @ref_type1 =~ ref
      y = ref.scan(REF_TYPE1) # BookName 1,1
      x = y[0]
      return "#{get_book_name(x[0], lang)} #{x[1]},#{x[2]}"
    end
    if @ref_type0 =~ ref
      y = ref.scan(REF_TYPE0) # BookName 1
      x = y[0]
      return "#{get_book_name(x[0], lang)} #{x[1]}"
    end
    ''
  end
end
