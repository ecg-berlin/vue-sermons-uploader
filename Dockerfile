FROM ruby:2.7

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/sermons

COPY Gemfile Gemfile.lock ./

RUN apt-get update && apt-get -y install cron libtag1-dev python-pip locales vim ffmpeg

ENV LANG C.UTF-8
ENV LANGUAGE C:C
ENV LC_ALL C.UTF-8
RUN locale-gen C.UTF-8
RUN dpkg-reconfigure locales

RUN pip install datadog
RUN bundle update --bundler && bundle install

COPY crontab.txt /etc/cron.d/sermons-cron
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

VOLUME /usr/data/backup
VOLUME /usr/data/output
VOLUME /usr/data/input
VOLUME /usr/data/add
VOLUME /usr/data/log

COPY . .

ENV sermonApiUrl="http://172.17.0.1/sermons/web" sermonSecretToken="asd" sermonPublicPath="http://localhost/sermons/web/downloads" datadogToken=""

ENTRYPOINT /entrypoint.sh

